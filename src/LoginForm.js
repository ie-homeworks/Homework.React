import axios from 'axios';
import { useState } from 'react';

function LoginForm() {
    
    const authUrl = "/auth/login";
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    
    return (
        <form className="Login-Form">
            Login: 
            <input type='text' name="login" onChange={(event) => setLogin(event.target.value)} />
            <br />
            Password: 
            <input type='password' name="pass" onChange={(event) => setPassword(event.target.value)} />
            <br />
            <button type='submit' onSubmit={async (login, password) =>{
                await axios.post(authUrl, {
                    login: login, 
                    password: password
                })
                    .then((resp) => alert(resp.statusText))
                    .catch((reason) => alert(reason));
            }}>Submit</button>
        </form>
    );
}

export default LoginForm;