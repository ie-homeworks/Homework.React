import logo from './logo.svg';
import './App.css';
import Footer from './Footer';
import LoginForm from './LoginForm';

function App() {
  const currentYear = new Date().getFullYear();
  const myName ='Igor Evstegneev'
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <LoginForm/>
        <br/>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <Footer year={currentYear} owner={myName}/>
    </div>
  );
}      

export default App;
