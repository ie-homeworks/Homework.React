import PropTypes from 'prop-types';

function Footer({year, owner}){
    return (
        <footer>
            <a href="/copyright">
                (C) {year}
                {owner
                ? <text>&nbsp;&nbsp;{owner}</text>
                : null}
                </a>
        </footer>
    )
}

Footer.defaultProps = {
    year: 0,
    owner: 'Name Surname'
}

Footer.propTypes = {
    year: PropTypes.number,
    owner: PropTypes.string
}

export default Footer;